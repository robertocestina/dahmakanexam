//
//  main.m
//  SampleObjectiveC
//
//  Created by MyDigi Accenture on 07/04/2018.
//  Copyright © 2018 MyDigi Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
