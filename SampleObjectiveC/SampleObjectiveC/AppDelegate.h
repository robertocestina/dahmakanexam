//
//  AppDelegate.h
//  SampleObjectiveC
//
//  Created by MyDigi Accenture on 07/04/2018.
//  Copyright © 2018 MyDigi Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

